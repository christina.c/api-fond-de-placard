import { RecipeRepository } from "../Repository/RecipeRepository";

export class RecipeBuisness {
    constructor() {

    }
    getRecipe() {
        let repo = new RecipeRepository();
        let result = repo.findAllRecipe();
        return result;
    }

    createRecipe(name: string, category: string, picture: string, score: number) {
        let repo = new RecipeRepository();
        let result = repo.CreateRecipe(name, category, picture, score,ingredient);
        return result;
    }

    searchRecipe(name: string) {
        let repo = new RecipeRepository();
        let result = repo.getByName(name);
        return result;
    }
    // pas terminé
    rateRecipe(id) {
        let repo = new RecipeRepository();
        let data = repo.getOne(id);
        repo.update();
    }
    proposeCorrection() {

    }
    validateCorrection() {

    } 
    addIngredient() {

    }

}
