import { RecipeBuisness } from "../Business/RecipeBusiness";


let Recipe = new RecipeBuisness();



exports.getRecipe = (req, res, next) => {
    return res.status(200).json({ status: 200, data: Recipe.getRecipe(), message: "Succesfully Recipes Retrieved" });
}

exports.getOneRecipe = (req, res, next) => {
    return res.status(200).json({ status: 200, data: Recipe.getOne(), Message: "Succesfully Recipe Retrieved"});
}

exports.createRecipe = (req, res, next) => {
    Recipe.createRecipe(...req, req, req, req, req);
    return res.status(201).json({status: 201, Message: "Succesfully created !"});
}

exports.searchRecipe = (req, res, next) => {
    let result = Recipe.searchRecipe(req);
    if (result) {
        return res.status(200).json({ status: 200, data: result, Message: "Succesfully Recipe Retrieved"});
    } else {
        return res.status(404).json({ status: 404, data: result, Message: "404 not found"});
    }
}