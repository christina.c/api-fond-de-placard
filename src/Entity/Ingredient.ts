

export class Ingredient {

    constructor( private name:string,
                 private id?:number){}

    getId(){
        return this.id;
    }
   
    getName(){
        return this.name;
    }

    setId(id:number){

        this.id=id;
    }

    setName(name:string){

        this.name = name;
    }

}