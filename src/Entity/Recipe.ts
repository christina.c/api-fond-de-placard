

export class Recipe {

    constructor( private name:string,
                 private category:string,
                 private picture:string,
                 private score:number,
                 private ingredient,
                 private id?:number){}

    getId(){
        return this.id;
    }
   
    getName(){
        return this.name;
    }

    getCategory(){
        return this.category;
    }

    getPicture(){
        return this.picture;
    }

    getScore(){
        return this.score;
    }

    getIngredient(){
        return this.ingredient;
    }

    setId(id:number){

        this.id=id;
    }

    setName(name:string){

        this.name = name;
    }

    setCategory(category){
        this.category = category;
    }

    setPicture(picture){

        this.picture = picture;
    }

    setScore(score:number){

        this.score=score;
    }


}
