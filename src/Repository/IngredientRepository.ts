

    import { BDDRepository} from "../Repository/BDDRepository";
    import { Ingredient } from "../Entity/Ingredient";
    
    
    
    export class IngredientRepository {
        private query;
    
        constructor() {

        let databaseConnexion = new BDDRepository();
        this.query = databaseConnexion.getQuery();
            
    }
    
   
        async findAllIngredient():Promise<Ingredient[]> {
            let result = await this.query("SELECT * FROM Ingredient");
            return result.map(row => new Ingredient(row['name'], row['id']));
        }
    
        async createÏngredient(ingredient:Ingredient):Promise<string> {
            let result = await this.query('INSERT INTO Ingredient (name) VALUES (?)', [
                ingredient.getName()
            ]);
            let id = result.insertId;
            ingredient.setId(id);
            return id;
        }

        async updateIngredient(ingredient:Ingredient){

            let changeIngredient = await this.query('UPDATE Recipe SET ingredient = "ingredient" WHERE !ingredient)')

            return changeIngredient;
        }

    async deleteIngredient(ingredient:Ingredient) :Promise<Ingredient[]> {

        let deleteIngredient = await this.query('DELETE FROM Ingredient WHERE id != id)');
        
        return deleteIngredient;

    }

}